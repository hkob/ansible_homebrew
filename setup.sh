#!/bin/sh
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
xcode-select --install
sudo chown -R $USER /usr/local
brew doctor
brew install ansible
