# ansible-homebrew #

Homebrew 関係を自動で設定してくれる ansible-playbook です。

### 何のためのリポジトリ ###

* Homebrew の brew を管理する
* MacTeX の 日本語設定を行う
* rbenv で ruby を管理する

### セットアップ方法 ###

* `sh setup.sh` を実行します。内容は以下の通りです。
    * homebrew のダウンロードとインストール
    * brew doctor (エラーが出ても特に何もしません)
    * ansible を brew でインストール
* `ansible-playbook -i hosts site.yml` として実行します
    * homebrew cask 関係のパッケージ設定
    * homebrew のパッケージ設定
    * rbenv で ruby をインストール

### 参考 ###

ansible 2.0 になって with_items の表示が一括で表示されるようになってしまったため、インストールパッケージごとにパッケージ名を表示できるように include することにしました(include 時に名前が出るため)。表示が面倒だと思うかもしれませんが、MacTeX など非常にダウンロードなどに時間がかかるものの場合、不安になると思うので、改修しました。また、atom は自分が使っていないので、ここからは外しました。

以前は、mactex の設定をここでやっていましたが、単体でインストールだけすることが多いので、

https://bitbucket.org/hkob/ansible_mactex

にて分離しました。ファイルだけ一部残っていますが、ここでは使っていません。